-module(pro251_home_controller, [Req, SessionId]).
-compile(export_all).

-define(SPAM_CHANNEL, "SpamChannel").
-define(FILE_READ_SIZE, 1024*1024*2).

hello('GET', []) ->
    ok.

spam('GET', [Timestamp]) ->
    {ok, NewTimestamp, NewMessages} = boss_mq:pull(?SPAM_CHANNEL, list_to_integer(Timestamp)),
    {json, [{timestamp, NewTimestamp}, {messages, NewMessages}]};
spam('GET', []) ->
    {ok, NewTimestamp, NewMessages} = boss_mq:pull(?SPAM_CHANNEL, now),
    {json, [{timestamp, NewTimestamp}, {messages, NewMessages}]};

spam('POST', []) ->
    boss_mq:push(?SPAM_CHANNEL, [{name, Req:post_param("name", "anonymous")}, {text, Req:post_param("text", "")}]),
    {output, ""}.

media('GET', []) ->
    ok.

videos('GET', []) ->
    try
	case filelib:wildcard(filename:join(["priv/media/videos", "*.mp4"])) of
	    [] ->
		ok;
	    Files when is_list(Files) ->
		{ok, [{files, Files}]}
	end
    catch
	throw:not_found ->
	    not_found
    end;

videos('GET', [Video]) ->
    try
	case filename:extension(Video) of
	    ".mp4" ->
		ok;
	    _ ->
		throw(not_found)
	end,
	case filelib:is_regular(filename:join(["priv/media/videos", Video])) of
	    false ->
		throw(not_found);
	    true ->
		{ok, [{video_file_name, Video}]}
	end
    catch
	throw:not_found ->
	    not_found
    end.

video_data('GET', [Filename]) ->
    Gen = fun(File) ->
		  case file:read(File,?FILE_READ_SIZE) of
		      {ok, Data} ->
			  {output, Data, File};
		      eof ->
			  file:close(File),
			  done
		  end
	  end,
    case file:open(filename:join(["priv/media/videos", Filename]), [read, binary]) of
	{error, Reason} ->
	    io:format("error opening file: ~p~n", [Reason]),
	    not_found;
	{ok, File} ->
	    {stream, Gen, File}
    end.

pictures('GET', []) ->
    try
	case filelib:wildcard(filename:join(["priv/media/pictures", "*"])) of
	    [] ->
		ok;
	    Files when is_list(Files) ->
		{ok, [{files, Files}]}
	end
    catch
	throw:not_found ->
	    not_found
    end;

pictures('GET', [Picture]) ->
    try
	case filelib:is_regular(filename:join(["priv/media/pictures", Picture])) of
	    false ->
		throw(not_found);
	    true ->
		{ok, [{file_name, Picture}]}
	end
    catch
	throw:not_found ->
	    not_found
    end.

picture_data('GET', [Filename]) ->
    Gen = fun(File) ->
		  case file:read(File,?FILE_READ_SIZE) of
		      {ok, Data} ->
			  {output, Data, File};
		      eof ->
			  file:close(File),
			  done
		  end
	  end,
    case file:open(filename:join(["priv/media/pictures", Filename]), [read, binary]) of
	{error, Reason} ->
	    io:format("error opening file: ~p~n", [Reason]),
	    not_found;
	{ok, File} ->
	    {stream, Gen, File, [{"Content-Type", mimetypes:extension(filename:extension(Filename))}]}
    end.

parse_range_request_header(undefined) ->
    undefined;
parse_range_request_header(String) ->
    ok.




contact('GET', []) ->
    ok.
