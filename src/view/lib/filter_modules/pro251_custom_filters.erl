-module(pro251_custom_filters).
-compile(export_all).

% put custom filters in here, e.g.
%
% my_reverse(Value) ->
%     lists:reverse(binary_to_list(Value)).
%
% "foo"|my_reverse   => "oof"

get_basename(Path) ->
    filename:basename(Path, ".mp4").
get_filename(Path) ->
    filename:basename(Path).

get_video_dir(Filename) ->
    get_video_dir_impl(filename:split(Filename)).

get_video_dir_impl([_Priv = "priv", _Videos = "videos", Dir, _BaseName]) ->
    Dir.
